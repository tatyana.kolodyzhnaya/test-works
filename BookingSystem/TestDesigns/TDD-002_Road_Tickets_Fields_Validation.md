<!--
     Test Design Author: Tatyana Kolodyazhnaya
     tatyana.kolodyzhnaya@gmail.com
 -->
<!--
    See this document on git
    https://gitlab.com/tatyana.kolodyzhnaya/test-works/blob/master/BookingSystem/TestDesigns/TDD-002_Road_Tickets_Fields_Validation.md
 -->
# Test Design `TDD-002_Road_Tickets_Fields_Validation`

## Requirements

Req002  The Ticketing service functionality:

1. The user should be able to choose options: one-way ticket or round-trip ticket, date of the trip and count of the passengers;
2. The system must show total details of the trip when the User selects a one-way trip type;
3. The system must show total details of the trip when the User selects a round trip ticket type;
4. After the User clicks the button "To pay" system should redirect the User to the page for the payment in USD.

## Approach Refinement

### Verification Approach

This test design covers the functionality of validations of the user input on the booking page of the ticket.

## Scenario `Road_Tickets_Fields_Validation_Redirect_To_Payment_Page_Positive_TS002_01`

```gherkin
Given the user enters the site
    And User opens page for booking of tickets
When the User selects <Ticket type>
    And the User fills all other mandatory fields with any valid data
    And the User clicks button "To pay"
Then the User is redirected to the page for the payment in USD
```

|**##**|**Ticket type**     |
| ----  | -------           |
| 1     | One-way ticket    |
| 2     | Round Trip Ticket |

## Scenario `Road_Tickets_Fields_Validation_Total_Information_Positive_TS002_02`

```gherkin
Given the user enters the site
     And User opens page for booking of the tickets
When the User selects <Ticket type>
     And the User fills all other mandatory fields with any valid data
Then the User sees all total information about the trip on this page
```

|**##**|**Ticket type**     |
| ----  | -------           |
| 1     | One-way ticket    |
| 2     | Round Trip Ticket |

## Scenario `Road_Tickets_Fields_Validation_Empty_Fields_Negative_TS002_03`

```gherkin
Given the User enters the site
    And the User opens page for booking of the tickets
When the User NOT fill all fields
Then the User doesn't see any information about the trip on the page
    And can't click button "To pay"
    And the User sees validation errors about all mandatory fields
```  

## Scenario `Road_Tickets_Discounts_Not_Selected_City_Negative_TS002_04`

```gherkin
Given the User enters the site
    And the User opens page for booking of the tickets
When the User selects any Ticket type
    And the User selects <Destination City>
    And the User selects any available date of the trip
    And the User selects the number of passengers that is more or equals to 1
Then the User doesn't see any information about the destination city and the price of the trip on the page
    And can't click button "To pay"
    And the User sees validation error with the message about wrong destination city of the trip
```

|**##**|**Destination City**                     |
| -    | -                                       |
| 1    | City of departure                       |
| 2    | Any city which is missing in the system |

## Scenario `Road_Tickets_Discounts_Not_Selected_Date_Negative_TS002_05`

```gherkin
Given the User enters the site
    And the User opens page for booking of the tickets
When the User selects any Ticket type
    And the User selects any available destination city
    And the User selects <Date Of The Trip>
    And the User selects the number of passengers that is more or equals to 1
Then the User doesn't see any information about the date and the price of the trip on the page
    And can't click button "To pay"
    And the User sees validation error with the message about wrong date of the trip
```

|**##**|**Date Of The Trip**                    |
| -    | -                                      |
| 1    | Date in the wrong format               |
| 2    | Past date                              |
| 3    | Any date from the not available future |

## Scenario `Road_Tickets_Discounts_Not_Selected_Count_Of_Passengers_Negative_TS002_06`

```gherkin
Given the User enters the site
    And the User opens page for booking of the tickets
When the User selects Round Trip Ticket
    And the User selects any available destination city
    And the User selects any available date of the trip
    And the User selects the <Count Of Passengers>
Then the User doesn't see any information about the count of passengers and the price of the trip on the page
    And can't click button "To pay"
    And the User sees validation error with the message about wrong count of the passengers of the trip
```

|**##**|**Count Of Passengers**                                         |
| -    | -                                                              |
| 1    | 0                                                              |
| 2    | -1                                                             |
| 3    | Any count that more than the available count of the passengers |