<!--
     Test Design Author: Tatyana Kolodyazhnaya
     tatyana.kolodyzhnaya@gmail.com
 -->
<!--
    See this document on git
    https://gitlab.com/tatyana.kolodyzhnaya/test-works/blob/master/BookingSystem/TestDesigns/TDD-001_Road_Tickets_Discounts.md
 -->
# Test Design `TDD-001_Road_Tickets_Discounts`

## Requirements

Req001 The discount functionality:

1. Discounts should be applied if three conditions are met:
     * The selected the option "round trip"
     * The ticket is more expensive than 199 USD
     * The trip must take place in winter

2. The discounts should be applied in this order:
     * 70% discount for all December departures
     * The additional discount of 50 USD for all trips in a December of the leap year
3. Each iteration of the accrual of discounts must meet the condition: the total price must not be less of the 10% from the original price

## Approach Refinement

### Verification Approach

This test design covers the functionality of charging discounts when booking tickets by the User.

## Scenario `Road_Tickets_Discounts_Positive_TS001_01`

```gherkin
Given the User enters the site
     And User opens page for booking of the tickets
When the User selects <Ticket type>
     And the User selects destination city where ticket price equals to <Ticket Price>
     And the User selects <Date Of Trip>
     And the User selects the <Number Of Passengers>
Then the User sees all total information about the trip with <Total ticket price> on this page
```

|**##**|**Ticket type**| **Ticket Price** |**Date Of Trip** |**Number Of Passengers** |**Total ticket price** |
| -    | -                 | -     | -               |-  |-     |  
| 1    | One-way ticket    | 200   |18 December 2019 |1  |200   |
| 2    | One-way ticket    | 210   |15 June 2019     |2  |420   |
| 3    | One-way ticket    | 199   |05 December 2019 |3  |597   |
| 4    | One-way ticket    | 180   |21 May 2019      |1  |180   |
| 5    | One-way ticket    | 80    |25 January 2019  |1  |80    |
| 6    | One-way ticket    | 630.75|14 January 2019  |6  |3784.5|
| 7    | One-way ticket    | 550   |03 February 2019 |2  |1100  |
| 8    | One-way ticket    | 40    |28 February 2019 |1  |40    |
| 9    | One-way ticket    | 250.50|18 December 2020 |5  |1252.5|
| 10   | One-way ticket    | 349   |20 August 2020   |2  |698   |
| 11   | One-way ticket    | 189   |31 December 2020 |1  |189   |
| 12   | One-way ticket    | 110   |15 April 2020    |4  |440   |
| 13   | One-way ticket    | 93    |25 January 2020  |1  |93    |
| 14   | One-way ticket    | 220   |03 February 2020 |6  |1320  |
| 15   | One-way ticket    | 35    |07 January 2020  |2  |70    |
| 16   | One-way ticket    | 360   |29 February 2020 |1  |360   |
| 13   | Round Trip Ticket | 175   |01 December 2019 |1  |175   |
| 14   | Round Trip Ticket | 55    |19 July 2019     |3  |165   |
| 15   | Round Trip Ticket | 536   |25 December 2019 |8  |1286.4|
| 16   | Round Trip Ticket | 420   |11 November 2019 |1  |420   |
| 17   | Round Trip Ticket | 200   |09 December 2020 |1  |60    |
| 18   | Round Trip Ticket | 365   |15 October 2020  |2  |730   |
| 19   | Round Trip Ticket | 100   |29 December 2020 |9  |900   |
| 20   | Round Trip Ticket | 30.6  |09 March 2020    |4  |122.4 |
| 21   | Round Trip Ticket | 300   |17 December 2020 |3  |220   |
| 22   | Round Trip Ticket | 600   |28 August 2020   |10 |6000  |
| 23   | Round Trip Ticket | 86    |03 January 2019  |1  |86    |
| 24   | Round Trip Ticket | 728   |15 January 2020  |3  |2184-Basic Discounts|
| 25   | Round Trip Ticket | 150   |28 February 2019 |7  |1050  |
| 26   | Round Trip Ticket | 1080  |29 February 2020 |1  |1080-Basic Discounts|
---
**NOTE**
The information about discounts for January and February months is not available in the descriptions and requirements for the system so it was decided to temporarily replace the variable value 'Basic Discounts'.

---

## Scenario `Road_Tickets_Discounts_Positive_TS001_02`

```gherkin
Given the user enters the site
     And User opens page for booking of the tickets
When the User selects <Ticket type>
     And the User selects destination city where ticket price equals to <Ticket Price>
     And the User selects <Date Of Trip>
     And the User selects the <Number Of Passengers>
     And the User clicks button "To pay"
Then the User is redirected to the page for the payment with <Total ticket price> in USD
```

|**##**|**Ticket type**| **Ticket Price** |**Date Of Trip** |**Number Of Passengers** |**Total ticket price** |
| -    | -                 | -     | -               |-  |-     |  
| 1    | One-way ticket    | 200   |18 December 2019 |1  |200   |
| 2    | One-way ticket    | 210   |15 June 2019     |2  |420   |
| 3    | One-way ticket    | 199   |05 December 2019 |3  |597   |
| 4    | One-way ticket    | 180   |21 May 2019      |1  |180   |
| 5    | One-way ticket    | 80    |25 January 2019  |1  |80    |
| 6    | One-way ticket    | 630.75|14 January 2019  |6  |3784.5|
| 7    | One-way ticket    | 550   |03 February 2019 |2  |1100  |
| 8    | One-way ticket    | 40    |28 February 2019 |1  |40    |
| 9    | One-way ticket    | 250.50|18 December 2020 |5  |1252.5|
| 10   | One-way ticket    | 349   |20 August 2020   |2  |698   |
| 11   | One-way ticket    | 189   |31 December 2020 |1  |189   |
| 12   | One-way ticket    | 110   |15 April 2020    |4  |440   |
| 13   | One-way ticket    | 93    |25 January 2020  |1  |93    |
| 14   | One-way ticket    | 220   |03 February 2020 |6  |1320  |
| 15   | One-way ticket    | 35    |07 January 2020  |2  |70    |
| 16   | One-way ticket    | 360   |29 February 2020 |1  |360   |
| 13   | Round Trip Ticket | 175   |01 December 2019 |1  |175   |
| 14   | Round Trip Ticket | 55    |19 July 2019     |3  |165   |
| 15   | Round Trip Ticket | 536   |25 December 2019 |8  |1286.4|
| 16   | Round Trip Ticket | 420   |11 November 2019 |1  |420   |
| 17   | Round Trip Ticket | 200   |09 December 2020 |1  |60    |
| 18   | Round Trip Ticket | 365   |15 October 2020  |2  |730   |
| 19   | Round Trip Ticket | 100   |29 December 2020 |9  |900   |
| 20   | Round Trip Ticket | 30.6  |09 March 2020    |4  |122.4 |
| 21   | Round Trip Ticket | 300   |17 December 2020 |3  |220   |
| 22   | Round Trip Ticket | 600   |28 August 2020   |10 |6000  |
| 23   | Round Trip Ticket | 86    |03 January 2019  |1  |86    |
| 24   | Round Trip Ticket | 728   |15 January 2020  |3  |2184-Basic Discounts|
| 25   | Round Trip Ticket | 150   |28 February 2019 |7  |1050  |
| 26   | Round Trip Ticket | 1080  |29 February 2020 |1  |1080-Basic Discounts|

---
**NOTE**
The information about discounts for January and February months is not available in the descriptions and requirements for the system so it was decided to temporarily replace the variable value 'Basic Discounts'.

---